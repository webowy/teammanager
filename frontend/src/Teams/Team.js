import React from 'react';
import { connect } from 'react-redux';
import * as actions from '../actions/players';
import {
  Link
} from 'react-router-dom'
import { CircularProgress } from 'material-ui/Progress';
import Grid from 'material-ui/Grid';
import List, {
  ListItem,
  ListItemIcon,
  ListItemText,
} from 'material-ui/List';
import Typography from 'material-ui/Typography';
import DirectionsRunIcon from 'material-ui-icons/DirectionsRun';
import { withStyles } from 'material-ui/styles';
import mainStyles from '../styles/mainStyles';

class Team extends React.Component {
  componentDidMount() {
    this.props.getPlayers(this.props.match.params.teamdId);
  }

  

  render() {
    const { classes } = this.props;
    return(
      <main>
        <Grid container  >
          <Grid item xs={12} md={12}>
            <Typography type="title" className={classes.title}>
               <DirectionsRunIcon /> Lista Zawodników
            </Typography>
          </Grid>
        <Grid item xs={12} md={6}>
           {isLoading(this.props.playersStore.isLoading)}
          <List style={{backgroundColor: "#fff", display: (this.props.playersStore.isLoading) ? 'none':''}}>
            {this.props.playersStore.players.map(item => {
                return (
                  <Link key={item.id} to={`/player/${item.id}`} className={classes.menuLink}>
                    <ListItem button>
                      <ListItemIcon>
                        <DirectionsRunIcon />
                      </ListItemIcon>
                      <ListItemText  primary={item.name}/>
                    </ListItem>
                  </Link>
                )   
              }) 
            }
          </List>
          </Grid>
          </Grid>
      </main>
    );

  }
}

const  isLoading = (isLoading) => {
  if(isLoading) {
    return <Grid item xs={12} md={12} style={{textAlign: "center"}}><CircularProgress /></Grid>
  }
 }

const mapStateToProps = (state) => {
  return {
    ...state
  };
}

const mapDispatchToProps = (dispatch) => {
  return {
    getPlayers: (teamId) => dispatch(actions.getPlayers(teamId))
  };
}


export default withStyles(mainStyles, { withTheme: true })(connect(mapStateToProps, mapDispatchToProps)(Team));