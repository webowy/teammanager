import React from 'react';
import { connect } from 'react-redux';
import * as actions from '../actions/teams';
import {
  Link
} from 'react-router-dom'
import { CircularProgress } from 'material-ui/Progress';
import Grid from 'material-ui/Grid';
import List, {
  ListItem,
  ListItemIcon,
  ListItemText,
} from 'material-ui/List';
import Typography from 'material-ui/Typography';
import FolderIcon from 'material-ui-icons/Folder';
import DirectionsRunIcon from 'material-ui-icons/DirectionsRun';
import { withStyles } from 'material-ui/styles';
import mainStyles from '../styles/mainStyles';

class Teams extends React.Component {
  componentDidMount() {
    this.props.getTeams();
  }

  render() {
    console.log(this.props);
    const { classes } = this.props;
    return(
      <main>
        <Grid container >
          <Grid item xs={12} md={12}>
            <Typography type="title" className={classes.title}>
               <DirectionsRunIcon /> Zespoły
            </Typography>
          </Grid>
        <Grid item xs={12} md={6}>
         {isLoading(this.props.teamsStore.isLoading)}
          
          <List style={{backgroundColor: "#fff", display: (this.props.teamsStore.isLoading) ? 'none':''}}>
            
            {this.props.teamsStore.teams.map(item => {
                return (
                  <Link key={item.id}  to={`/teams/${item.id}`} className={classes.menuLink}>
                    <ListItem button>
                      <ListItemIcon>
                        <FolderIcon />
                      </ListItemIcon>
                      <ListItemText  primary={item.name}/>
                    </ListItem>
                  </Link>
                )   
              }) 
            }
          </List>
          </Grid>
          </Grid>
      </main>
    );

  }
}

const  isLoading = (isLoading) => {
  if(isLoading) {
    return <Grid item xs={12} md={12} style={{textAlign: "center"}}><CircularProgress /></Grid>
  }
 }

const mapStateToProps = (state) => {
  return {
    ...state
  };
}

const mapDispatchToProps = (dispatch) => {
  return {
    setData: (data) => dispatch(actions.setData(data)),
    getTeams: () => dispatch(actions.getTeams())
  };
}


export default withStyles(mainStyles, { withTheme: true })(connect(mapStateToProps, mapDispatchToProps)(Teams));