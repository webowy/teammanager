/* eslint-disable flowtype/require-valid-file-annotation */
import React from 'react';
import PropTypes from 'prop-types';
import { withStyles } from 'material-ui/styles';
import Cookies from 'universal-cookie';


import mainStyles from './styles/mainStyles';

import {
  BrowserRouter as Router,
  Route,
  Switch,
  Redirect
} from 'react-router-dom';
import Login from './User/Login';
import Dashboard from './Dashboard/Dashboard';
import Teams from './Teams/Teams';
import Team from './Teams/Team';
import Player from './Players/Player';
import Obligation from './Players/Obligation';
import MainAppBar from './Layout/MainAppBar';
import SideBar from './Layout/SideBar';
import Authentication from './User/authHOC'


class App extends React.Component {
  state = {
    open: false,
    isLogged: true,
    redirect: null
  };
  componentDidMount() {
    
  }

  handleDrawerOpen = () => {
    this.setState({ open: true });
  };

  handleDrawerClose = () => {
    this.setState({ open: false });
  };


  render() {
    const { classes } = this.props;

    return (
        <Router id="hi">  
           
            <div className={classes.root}>
            {this.state.redirect}
              <Switch>
               <Route exact path="/login" component={Login} />
               <Route path="" render={props => (
                  <div className={classes.appFrame} >
                      <MainAppBar open={this.state.open} handleDrawerOpen={() => {this.handleDrawerOpen()}} />
                      <SideBar  open={this.state.open} handleDrawerClose={() => {this.handleDrawerClose()}}/> 
                      <main className={classes.content}>
                          <div>
                              
                              <Route exact path="/" component={Authentication(Dashboard)} />
                              <Route exact path="/teams" component={Authentication(Teams)} />
                              <Route path="/teams/:teamdId" component={Authentication(Team)} />  
                              <Route path="/player/:playerId" component={Authentication(Player)} />  
                              <Route path="/obligation/:obligationId" component={Authentication(Obligation)} /> 
                          </div>
                      </main>
                  </div>
                )}/>
              </Switch>
            </div>
        </Router>
    );
  }
}

App.propTypes = {
  classes: PropTypes.object.isRequired,
  theme: PropTypes.object.isRequired,
};

export default withStyles(mainStyles, { withTheme: true })(App);