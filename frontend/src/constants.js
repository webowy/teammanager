//export const HOST = 'http://192.168.8.100';
export const HOST = 'http://localhost';

export const SET_ID = 'SET_ID';
export const GET_DATA = 'GET_DATA';
export const UPDATE_ID = 'UPDATE_ID';


export const POST_LOGIN = 'POST_LOGIN';
export const LOGIN_POST_START = 'LOGIN_POST_START';
export const LOGIN_POST_SUCCESS = 'LOGIN_POST_SUCCESS';
export const LOGIN_POST_ERROR = 'LOGIN_POST_ERROR';

export const TEAMS_GET_START = 'TEAMS_GET_START';
export const TEAMS_GET_SUCCESS = 'TEAMS_GET_SUCCESS';
export const TEAMS_GET_ERROR = 'TEAMS_GET_ERROR';

export const PLAYERS_GET_START = 'PLAYERS_GET_START';
export const PLAYERS_GET_SUCCESS = 'PLAYERS_GET_SUCCESS';
export const PLAYERS_GET_ERROR = 'PLAYERS_GET_ERROR';

export const PLAYER_GET_START = 'PLAYER_GET_START';
export const PLAYER_GET_SUCCESS = 'PLAYER_GET_SUCCESS';
export const PLAYER_GET_ERROR = 'PLAYER_GET_ERROR';

export const OBLIGATIONS_GET_START = 'OBLIGATIONS_GET_START';
export const OBLIGATIONS_GET_SUCCESS = 'OBLIGATIONS_GET_SUCCESS';
export const OBLIGATIONS_GET_ERROR = 'OBLIGATIONS_GET_ERROR';

export const OBLIGATION_GET_START = 'OBLIGATION_GET_START';
export const OBLIGATION_GET_SUCCESS = 'OBLIGATION_GET_SUCCESS';
export const OBLIGATION_GET_ERROR = 'OBLIGATION_GET_ERROR';
export const OBLIGATION_UPDATE = 'OBLIGATION_UPDATE';

export const PAYMENT_POST_START = 'PAYMENT_POST_START';
export const PAYMENT_POST_SUCCESS = 'PAYMENT_POST_SUCCESS';
export const PAYMENT_POST_ERROR = 'PAYMENT_POST_ERROR';
export const LOGOUT = "LOGOUT";
