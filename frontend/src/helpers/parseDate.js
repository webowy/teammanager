export const parseDate = (date) => {
    
        let dateObj = new Date(date);
        let monthNames = [
            "Styczeń", "Luty", "Marzec",
            "Kwiecień", "Maj", "Czerwiec", "Lipiec",
            "Sierpień", "Wrzesień", "Październik",
            "Listopad", "Grudzueń"
        ];
    
        let day = dateObj.getDate();
        let monthIndex = dateObj.getMonth();
        let year = dateObj.getFullYear();
    
        return day + ' ' + monthNames[monthIndex] + ' ' + year;
}

