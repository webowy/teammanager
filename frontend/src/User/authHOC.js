import React, { Component } from 'react';
import {
    Redirect
  } from 'react-router-dom';
  import Cookies from 'universal-cookie';
  import { connect } from 'react-redux';
  import PropTypes from 'prop-types';
  import * as actions from '../actions/login';


export default function (ComposedComponent) {
    const cookies = new Cookies();
    
    class Authentication extends Component {
        static contextTypes = {
            router: PropTypes.object
        }

        componentWillMount() {
            const cookies = new Cookies();
            let token =  cookies.get('app_token', { path: '' });   

            if(token === null || token == undefined) {
               this.props.setLogut();
               this.context.router.history.push('/login');
            }
        }
       
        render() {
                return <ComposedComponent {...this.props} />
        }


    }

    const mapStateToProps = (state) => {
        return {
          ...state
        };
      }

      const mapDispatchToProps = (dispatch) => {
        return {
            setLogut: (data) => dispatch(actions.setLogout())
        };
    }
    
        

    return connect(mapStateToProps, mapDispatchToProps)(Authentication);
}

