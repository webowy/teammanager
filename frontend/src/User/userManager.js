import Cookies from 'universal-cookie';

const cookies = new Cookies();

export function login(token) { 
    cookies.set('app_token', token, { path: '/' });  
}
export function logout() {
    cookies.set('app_token', '',{ path: '/' });
    cookies.remove('app_token', { path: '/' });
}
export function getToken() {
   return cookies.get('app_token', { path: '/' });
}