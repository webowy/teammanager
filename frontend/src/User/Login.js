import React from 'react';
import { connect } from 'react-redux';
import { Redirect } from 'react-router';
import * as actions from '../actions/login';

import * as userManager from './userManager';

import TextField from 'material-ui/TextField';
import Grid from 'material-ui/Grid';
import Paper from 'material-ui/Paper';
import Button from 'material-ui/Button';
import { LinearProgress } from 'material-ui/Progress';




import { withStyles } from 'material-ui/styles';
import mainStyles from '../styles/mainStyles';


class Login extends React.Component {
  state = {
      username: "",
      password: "",
      redirect: null
  }
  componentDidUpdate() {
    if(typeof this.props.loginStore.token !== 'undefined' && this.props.loginStore.token !==null ){
        userManager.login(this.props.loginStore.token); 
        this.setState({redirect : <Redirect to='/'/>});
    }
  }
  authenticate(username, password) {
    this.props.postLogin(username, password);
  }

  handleChange = name => event => {
    this.setState({
      [name]: event.target.value,
    });
  };
  loader(isLoading) {
      if (isLoading) {
          return <LinearProgress style={{width: "100%", position: "absolute", bottom: "0", left: "0"}} />
      }
  }

  render() {
    const { classes } = this.props;

    let errorMessage  = null;
   
    if(this.props.loginStore.payload.data.message!==null) {
        errorMessage = 
        <Grid item xs={12} className={classes.loginError}>
            {this.props.loginStore.payload.data.message}
        </Grid>
    }

    return(
            <div className={classes.appFrame}>   
            {this.state.redirect}
            <Grid container spacing={20} justify="center"  alignItems="center"> 
                <Paper className={classes.login}>
                
                    <form className={classes.container} noValidate autoComplete="off">
                        <Grid item xs={12}>
                            <TextField
                            id="login"
                            label="Login"
                            margin="normal"
                            value={this.state.username}
                            onChange={this.handleChange('username')}
                            />
                        </Grid>
                        <Grid item xs={12}>
                            <TextField
                            id="password"
                            label="Password"
                        
                            type="password"
                            autoComplete="current-password"
                            margin="normal"
                            value={this.state.password}
                            onChange={this.handleChange('password')}
                            />
                        </Grid>
                        <Grid item xs={12}>
                            <Button className={classes.LoginButton} raised color="primary" onClick={() => this.authenticate(this.state.username, this.state.password)}>
                                Login
                            {/* <Send className={classes.rightIcon} /> */}
                            </Button>
                        </Grid>
                        {errorMessage}
                    </form>
                    {this.loader(this.props.loginStore.isLoading)}
                </Paper>
               
            </Grid>
            </div>
    );

  }
}

 const mapStateToProps = (state) => {
  return {
    ...state
  };
}

const mapDispatchToProps = (dispatch) => {
  return {
    postLogin: (username, password) => dispatch(actions.postLogin(username, password))
  };
}

export default withStyles(mainStyles, { withTheme: true })(connect(mapStateToProps, mapDispatchToProps)(Login));