import React from 'react';
import { connect } from 'react-redux';
import * as actions from '../actions/obligation';
import * as actionsPayment from '../actions/payment';
import AddObligation from './AddObligation'

import { CircularProgress } from 'material-ui/Progress';
import Card from 'material-ui/Card';
import Typography from 'material-ui/Typography';
import Grid from 'material-ui/Grid';
import Table, { TableBody, TableCell, TableHead, TableRow } from 'material-ui/Table';
import Button from 'material-ui/Button';
import Tooltip from 'material-ui/Tooltip';

import AddIcon from 'material-ui-icons/Add';
import MonetizationOnIcon from 'material-ui-icons/MonetizationOn';
import DateRangeIcon from 'material-ui-icons/DateRange';
import PlaylistAddCheckIcon from 'material-ui-icons/PlaylistAddCheck';

import { withStyles } from 'material-ui/styles';
import mainStyles from '../styles/mainStyles';

import { parseDate } from '../helpers/parseDate';

class Obligation extends React.Component {
    state = {
        addObligationOpened: false,
    };

    componentDidMount() {
        this.props.getObligation(this.props.match.params.obligationId);
    }

    handleClickOpen = () => {
        this.setState({ addObligationOpened: true });
    };

    handleRequestClose = (data) => {
        this.setState({ addObligationOpened: false });
        this.props.setPayment(this.props.match.params.obligationId, data);
        
    };

    render() {
        const { classes } = this.props;
         return(
            <main>
                {isLoading(this.props.obligationStore.isLoading)}
                         <Grid container  >
                             <Grid item xs={12} md={6} style={{ }}>
                                <Card style={{padding: "20px 20px 20px 20px", display: (this.props.obligationStore.isLoading) ? 'none':''}}>
                                    <Typography type="subheading" component="p">
                                        <MonetizationOnIcon style={{ height: "16px", marginBottom: "-2px"}} /> 
                                        Termin płatności:  <strong>{parseDate(this.props.obligationStore.obligation.obligationDate)} </strong>
                                    </Typography>
                                    <Typography type="subheading" component="p">
                                        <DateRangeIcon style={{ height: "16px", marginBottom: "-2px"}} /> 
                                        Kwota: <strong>{this.props.obligationStore.obligation.obligationValue + "zł"}</strong>
                                    </Typography>
                                </Card>
                            </Grid>

                            <Grid item xs={12} md={12} style={{ }}>
                                <Card style={{padding: "20px 0px 20px 0px", display: (this.props.obligationStore.isLoading) ? 'none':''}}>
                                    <Typography type="title" component="p" style={{padding: "0px 20px 0px 20px"}}>
                                        <PlaylistAddCheckIcon style={{ height: "22px", marginBottom: "-4px"}} /> Płatności
                                    </Typography>
                                    <Table className={classes.table}>
                                        <TableHead>
                                            <TableRow>
                                                <TableCell>Create Date</TableCell>
                                                <TableCell numeric>Amount (PLN)</TableCell>
                                                <TableCell numeric>Type</TableCell>
                                            </TableRow>
                                        </TableHead>
                                        <TableBody>
                                            {console.log(this.props.obligationStore.obligation.payments)}
                                        {this.props.obligationStore.obligation.payments.map(item => {
                                                return(
                                                    <TableRow key={item.id}>
                                                        <TableCell>{parseDate(item.createDate)}</TableCell>
                                                        <TableCell numeric>{item.amount}</TableCell>
                                                        <TableCell numeric>{item.type}</TableCell>
                                                        
                                                    </TableRow>

                                                );
                                            
                                            }) 
                                        }
                                    </TableBody>
                                    </Table>
                                </Card>
                            </Grid>

                         </Grid>
                         <Tooltip placement="left" title="Dodaj płatność">
                            <Button fab color="primary" aria-label="add" className={classes.button} onClick={this.handleClickOpen}>
                                <AddIcon />
                            </Button>
                        </Tooltip>
                        <AddObligation addObligationOpened={this.state.addObligationOpened} handleRequestClose={this.handleRequestClose} />
            </main>
         );
    }
}



const  isLoading = (isLoading) => {
    if(isLoading) {
        return <Grid item xs={12} md={12} style={{textAlign: "center"}}><CircularProgress /></Grid>
    }
}

const mapStateToProps = (state) => {
    return {
        ...state
    };
}

const mapDispatchToProps = (dispatch) => {
    return {
        getObligation: (obligationId) => dispatch(actions.getObligation(obligationId)),
        setPayment: (obligationId, data) => dispatch(actionsPayment.setPayment(obligationId, data))
    };
}


export default withStyles(mainStyles, { withTheme: true })(connect(mapStateToProps, mapDispatchToProps)(Obligation));