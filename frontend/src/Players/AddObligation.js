import React from 'react';
import { connect } from 'react-redux';
import * as actions from '../actions/obligation';

import Button from 'material-ui/Button';

import Dialog, {
  DialogActions,
  DialogContent,
  DialogTitle,
} from 'material-ui/Dialog';
import { MenuItem } from 'material-ui/Menu';
import Select from 'material-ui/Select';
import { FormControl } from 'material-ui/Form';
import Input, { InputLabel, InputAdornment } from 'material-ui/Input';

import { withStyles } from 'material-ui/styles';
import mainStyles from '../styles/mainStyles';

class AddObligation extends React.Component {
    state = {
        amount: 60,
        paymentType: "",
    }
    handleChange = event => {
        console.log(event.target);
        this.setState({ [event.target.name]: event.target.value });
    };

    render() {
        const { classes } = this.props;
         return(
                        <Dialog open={this.props.addObligationOpened} onRequestClose={this.props.handleRequestClose}>
                            <DialogTitle>Dodaj Płatność</DialogTitle>
                            <DialogContent>
                                <FormControl className={classes.formControl}>
                                    <InputLabel htmlFor="amount">Kwota</InputLabel>
                                    <Input
                                        name="amount"
                                        label=""
                                        id="amount"
                                        value={this.state.amount}
                                        onChange={this.handleChange}
                                        startAdornment={<InputAdornment position="start">$</InputAdornment>}
                                    /><br />
                                    </FormControl>
                                    <FormControl className={classes.formControl}>
                                        <InputLabel htmlFor="paymentType-simple">Typ</InputLabel>
                                        <Select
                                            value={this.state.paymentType}
                                            onChange={this.handleChange}                                    
                                            input={<Input name="paymentType" id="paymentType-simple" />}
               
                                        
                                        >
                                            <MenuItem value="">
                                                <em>None</em>
                                            </MenuItem>
                                            <MenuItem value={1}>Gotówka</MenuItem>
                                            <MenuItem value={2}>Przelew</MenuItem>
                                        </Select>

                                   

                                </FormControl>
                            </DialogContent>
                            <DialogActions>
                                <Button onClick={this.props.handleRequestClose} color="primary">
                                Anuluj
                                </Button>
                                <Button onClick={this.props.handleRequestClose.bind(this, this.state)} color="primary">
                                Dodaj
                                </Button>
                            </DialogActions>
                    </Dialog>
         );
    }
}

const mapStateToProps = (state) => {
    return {
        ...state
    };
}

const mapDispatchToProps = (dispatch) => {
    return {
       
    };
}


export default withStyles(mainStyles, { withTheme: true })(connect(mapStateToProps, mapDispatchToProps)(AddObligation));