import React from 'react';
import { connect } from 'react-redux';
import * as actions from '../actions/player';
import * as actionsObligations from '../actions/obligations';

import { Link } from 'react-router-dom';

import { CircularProgress } from 'material-ui/Progress';
import Card from 'material-ui/Card';
import Typography from 'material-ui/Typography';
import Grid from 'material-ui/Grid';
import MoneyIcon from 'material-ui-icons/MonetizationOn';
import Switch from 'material-ui/Switch';
import Divider from 'material-ui/Divider';

import List, {
  ListItem,
  ListItemIcon,
  ListItemSecondaryAction,
  ListItemText,
  ListSubheader
} from 'material-ui/List';


import { withStyles } from 'material-ui/styles';
import mainStyles from '../styles/mainStyles';

import { parseDate } from '../helpers/parseDate'

class Player extends React.Component {
  componentDidMount() {
    this.props.getPlayer(this.props.match.params.playerId);
    this.props.getObligations(this.props.match.params.playerId);
  }

  render() {
    const { classes } = this.props;
    return(
        <main>
            {isLoading(this.props.playerStore.isLoading)}
            <Card style={{ display: (this.props.playerStore.isLoading) ? 'none':''}}>
                <Grid container  >
                    <Grid item xs={12} md={12}>
                        <Typography type="title" className={classes.title}>
                         {this.props.playerStore.player.name} {this.props.playerStore.player.surname}
                        </Typography>

                        <Typography type="body1" className={classes.pos}>
                         <strong>Adres zamieszkania:</strong>
                        </Typography>
                        <Typography type="body1" className={classes.pos}>
                         ul. {this.props.playerStore.player.street} {this.props.playerStore.player.streetNumber}
                        </Typography>
                        <Typography type="body1" className={classes.pos}>
                         {this.props.playerStore.player.city} {this.props.playerStore.player.postCode}
                        </Typography>
                        <Typography type="body1" className={classes.pos}>
                         Data urodzenia:
                        </Typography>
                        <Typography type="body1" className={classes.pos}>
                         { parseDate(this.props.playerStore.player.birth) }
                        </Typography>
                    </Grid>
                </Grid>
            </Card>
            {isLoading(this.props.obligationsStore.isLoading)}
            <Card style={{marginTop: "20px", display: (this.props.obligationsStore.isLoading) ? 'none':''}}>
                <Grid container  >
                    
                    <Grid item xs={12} md={12}>
                    <List subheader={<ListSubheader>Zobowiązania</ListSubheader>}>
                    <Divider light />
                        {this.props.obligationsStore.obligations.map(item => {
                            return (
                                <Link  key={item.id} to={`/obligation/${item.id}`}>
                                <ListItem button>
                                    <ListItemIcon>
                                        <MoneyIcon />
                                    </ListItemIcon>
                                    <ListItemText  primary={parseDate(item.obligationDate)} secondary={item.obligationValue + "zł"}/>
                                    <ListItemSecondaryAction>
                                    <Switch
                                       
                                        checked={true}
                                    />
                                    </ListItemSecondaryAction>
                                </ListItem>
                                <Divider light />
                                </Link>
                            )   
                        }) 
                        }
                    </List>
                    </Grid>
                    </Grid>
            </Card>
        </main>
    );

  }
}

const  isLoading = (isLoading) => {
  if(isLoading) {
    return <Grid item xs={12} md={12} style={{textAlign: "center"}}><CircularProgress /></Grid>
  }
 }

 const mapStateToProps = (state) => {
  return {
    ...state
  };
}

const mapDispatchToProps = (dispatch) => {
  return {
    getPlayer: (playerId) => dispatch(actions.getPlayer(playerId)),
    getObligations: (playerId) => dispatch(actionsObligations.getObligations(playerId))
  };
}

export default withStyles(mainStyles, { withTheme: true })(connect(mapStateToProps, mapDispatchToProps)(Player));