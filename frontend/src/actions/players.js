import * as constants from '../constants';
import fetch from 'isomorphic-fetch';
import * as userManager from './../User/userManager';

export function getPlayersStart() {
  return {
    type: constants.PLAYERS_GET_START
  }
}

export function getPlayersSuccess(data) {
  return {
    type: constants.PLAYERS_GET_SUCCESS,
    payload: {
      data
    }
  }
}

export function getPlayersError(error) {
  return {
    type: constants.PLAYERS_GET_ERROR,
    payload: {
      error
    }
  }
}

export function getPlayers(teamId) {
  return (dispatch) => {
    dispatch(getPlayersStart());

    fetch(constants.HOST + '/app_dev.php/api/players?_team='+teamId, {
      headers: {
        Authorization: 'Bearer ' + userManager.getToken()
      }
    })
    .then(response => response.json())
    .then(data => dispatch(getPlayersSuccess(data)))
    .catch(error => dispatch(getPlayersError(error)));
  }
}