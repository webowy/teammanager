import * as constants from '../constants';
import fetch from 'isomorphic-fetch';
import * as userManager from './../User/userManager';

export function getPlayerStart() {
  return {
    type: constants.PLAYER_GET_START
  }
}

export function getPlayerSuccess(data) {
  return {
    type: constants.PLAYER_GET_SUCCESS,
    payload: {
      data
    }
  }
}

export function getPlayerError(error) {
  return {
    type: constants.PLAYER_GET_ERROR,
    payload: {
      error
    }
  }
}

export function getPlayer(playerId) {
  return (dispatch) => {
    dispatch(getPlayerStart());

    fetch(constants.HOST + '/app_dev.php/api/player/'+playerId, {
      headers: {
        Authorization: 'Bearer ' + userManager.getToken()
      }
    })
    .then(response => response.json())
    .then(data => dispatch(getPlayerSuccess(data)))
    .catch(error => dispatch(getPlayerError(error)));
  }
}