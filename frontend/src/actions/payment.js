import * as constants from '../constants';
import fetch from 'isomorphic-fetch';
import { updateObligation } from './obligation';
import * as userManager from './../User/userManager';

export function setPaymentStart() {
  return {
    type: constants.PAYMENT_POST_START
  }
}

export function setPaymentSuccess(data) {
  return {
    type: constants.PAYMENT_POST_SUCCESS,
    payload: {
      data
    }
  }
}

export function setPaymentError(error) {
  return {
    type: constants.PAYMENT_POST_ERROR,
    payload: {
      error
    }
  }
}

export function setPayment(obligationId, data) {
  return (dispatch) => {
    dispatch(setPaymentStart());

    fetch(constants.HOST + '/app_dev.php/api/payment',
          {
            method: "POST",
            body: JSON.stringify({
              obligation: obligationId,
              amount: data.amount,
              type: data.paymentType
            }),
            headers: {
              "Content-Type": "application/json",
              Authorization: 'Bearer ' + userManager.getToken()
            },
          })
    .then(response => response.json())
    .then(data => dispatch(setPaymentSuccess(data)))
    .then(data => dispatch(updateObligation(obligationId, data.payload.data)))
    .catch(error => dispatch(setPaymentError(error)));
  }
}