import * as constants from '../constants';
import fetch from 'isomorphic-fetch';
import * as userManager from './../User/userManager';

export function getObligationStart() {
  return {
    type: constants.OBLIGATION_GET_START
  }
}

export function getObligationSuccess(data) {
  return {
    type: constants.OBLIGATION_GET_SUCCESS,
    payload: {
      data
    }
  }
}

export function getObligationError(error) {
  return {
    type: constants.OBLIGATION_GET_ERROR,
    payload: {
      error
    }
  }
}

export function getObligation(obligationId) {
  return (dispatch) => {
    dispatch(getObligationStart());

    fetch(constants.HOST + '/app_dev.php/api/obligation/'+obligationId,  {
      headers: {
        Authorization: 'Bearer ' + userManager.getToken()
      }
    })
    .then(response => response.json())
    .then(data => dispatch(getObligationSuccess(data)))
    .catch(error => dispatch(getObligationError(error)));
  }
}

export function updateObligation(obligationId, data) {
  return {
    type: constants.OBLIGATION_UPDATE,
    payload: {
      data
    }
  }
}