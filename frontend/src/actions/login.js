import * as constants from '../constants';
import fetch from 'isomorphic-fetch';

export function setLogin(dataId) {
  return {
    type: constants.POST_LOGIN,
    payload: { 
      id: dataId
    } 
  };
}

export function postLoginStart() {
  return {
    type: constants.LOGIN_POST_START
  }
}

export function postLoginSuccess(data) {
  return {
    type: constants.LOGIN_POST_SUCCESS,
    payload: {
      data
    }
  }
}

export function postLoginError(error) {
  return {
    type: constants.LOGIN_POST_ERROR,
    payload: {
      error
    }
  }
}

export function setLogout() {
  return {
    type: constants.LOGOUT,
    payload: {
      logout: true
    }
  }
}

export function postLogin(username, password) {
  return (dispatch) => {
    dispatch(postLoginStart());

    fetch(constants.HOST + '/app_dev.php/api/login_check', {
        method: "POST",
        body: JSON.stringify({
            username: username,
            password: password,
          }),
          headers: {
            "Content-Type": "application/json"
          },
    })
    .then(response => response.json())
    .then(data => dispatch(postLoginSuccess(data)))
    .catch(error => dispatch(postLoginError(error)));
  }
}