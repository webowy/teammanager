import * as constants from '../constants';
import fetch from 'isomorphic-fetch';
import * as userManager from './../User/userManager';

export function getObligationsStart() {
  return {
    type: constants.OBLIGATIONS_GET_START
  }
}

export function getObligationsSuccess(data) {
  return {
    type: constants.OBLIGATIONS_GET_SUCCESS,
    payload: {
      data
    }
  }
}

export function getObligationsError(error) {
  return {
    type: constants.OBLIGATIONS_GET_ERROR,
    payload: {
      error
    }
  }
}

export function getObligations(playerId) {
  return (dispatch) => {
    dispatch(getObligationsStart());

    fetch(constants.HOST + '/app_dev.php/api/obligations?_player='+playerId, {
      headers: {
        Authorization: 'Bearer ' + userManager.getToken()
      }
    })
    .then(response => response.json())
    .then(data => dispatch(getObligationsSuccess(data)))
    .catch(error => dispatch(getObligationsError(error)));
  }
}