import * as constants from '../constants';
import fetch from 'isomorphic-fetch';
import axios from 'axios';
import * as userManager from './../User/userManager';

export function setData(dataId) {
  return {
    type: constants.SET_ID,
    payload: { 
      id: dataId
    } 
  };
}

export function getTeamsStart() {
  return {
    type: constants.TEAMS_GET_START
  }
}

export function getTeamsSuccess(data) {
  return {
    type: constants.TEAMS_GET_SUCCESS,
    payload: {
      data
    }
  }
}

export function getTeamsError(error) {
  console.log(error)
  return {
    type: constants.TEAMS_GET_ERROR,
    payload: {
      error
    }
  }
}

export function getTeams() {
  console.log('Bearer ' + userManager.getToken());
  return (dispatch) => {
    dispatch(getTeamsStart());

    axios.get(constants.HOST + '/app_dev.php/api/team', {
      headers: {
        "Content-Type" : "application/json",
        "Authorization": 'Bearer ' + userManager.getToken()
      }
    })
    .then(response => dispatch(getTeamsSuccess(response.data)))
    .catch(error => dispatch(getTeamsError(error)));
  }
}