import App from './App';
import chai, { expect } from 'chai';
import chaiJquery from 'chai-jquery';
import $ from 'jquery'

chai.use(chaiJquery);

describe('App' , () => {
  let component;

  it('App exist', ()=> {
    var $elem = $("<div id=\"hi\" foo=\"bar time\" />");

     expect($elem).should.have.attr('foo');
  })

  // it('renders something', () => {
  //   expect(component).to.exist;
  // });
});
