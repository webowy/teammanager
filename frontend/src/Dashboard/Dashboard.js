import React from 'react';
import { connect } from 'react-redux';
import * as actions from '../actions/teams';


import {
  Link
} from 'react-router-dom'


class  Dashboard extends React.Component  {

  constructor(props) {
    super(props);
    console.log(props);
    
  }

  componentDidMount(){
    this.props.getTeams();
  }


  onSetDataClick() {
    this.props.setData(this.props.id);
  }

  render() {
   
    return (
      
      <main>
       
        <ul>
           {this.props.teamsStore.teams.map(item => {
                return (<li key={item.id}>
                  Nazwa: <strong>{item.name}</strong>, 
                  Okres składki: {item.contribution_period},
                  Wartość Składki: {item.contribution_val} zł
                </li>)   
            }) 
          }
        </ul>
        <Link to={'/items/' + 1}>Pierwszy "item"</Link>
        <button onClick={this.onSetDataClick.bind(this)}>Set Data</button>
      </main>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    ...state
  };
}

const mapDispatchToProps = (dispatch) => {
  return {
    setData: (data) => dispatch(actions.setData(data)),
    getTeams: () => dispatch(actions.getTeams())
  };
}


export default connect(mapStateToProps, mapDispatchToProps)(Dashboard);