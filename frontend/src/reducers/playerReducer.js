import * as constants from '../constants';

const initalState = {
    player: [],
    isError: false,
    isLoading: false,
    payload: { 
      id: 0
    }
};

export const playerReducer = (state = initalState, action) => {
  switch (action.type) {
    case constants.PLAYER_GET_START:
      return { ...state, isLoading : true};
    case constants.PLAYER_GET_SUCCESS:
      return { ...state, isLoading: false, player: action.payload.data[0]};
    case constants.PLAYER_GET_ERROR:
      return { ...state, isLoading: false, isError: true};
    default:
      return state;
  }
};