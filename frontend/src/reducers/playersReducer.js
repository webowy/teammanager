import * as constants from '../constants';

const initalState = {
    players: [],
    isError: false,
    isLoading: false,
    payload: { 
      id: 0
    }
};

export const playersReducer = (state = initalState, action) => {
  switch (action.type) {
    case constants.PLAYERS_GET_START:
      return { ...state, isLoading : true};
    case constants.PLAYERS_GET_SUCCESS:
      return { ...state, isLoading: false, players: action.payload.data};
    case constants.PLAYERS_GET_ERROR:
      return { ...state, isLoading: false, isError: true};
    default:
      return state;
  }
};