import * as constants from '../constants';

const initalState = {
    user: [],
    isError: false,
    isLoading: false,
    payload: { 
      data: 
      {
        message: null,
        code: null,
       
      }
    },
    token: null
};

export const loginReducer = (state = initalState, action) => {
  switch (action.type) {
    case constants.LOGIN_POST_START:
      return { ...state, isLoading : true};
    case constants.LOGIN_POST_SUCCESS:
      return { ...state, isLoading: false, payload: action.payload, token: action.payload.data.token};
    case constants.LOGIN_POST_ERROR:
      return { ...state, isLoading: false, isError: true};
    case constants.LOGOUT:
      return { ...state, isLoading: false, isError: true, token: null};
    default:
      return state;
  }
};