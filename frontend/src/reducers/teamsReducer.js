import * as constants from '../constants';

const initalState = {
    teams: [],
    isError: false,
    logout: false,
    isLoading: false,
    payload: { 
      id: 0
    }
};

export const teamsReducer = (state = initalState, action) => {
  switch (action.type) {
    case constants.SET_ID:
      return { ...state, payload : {id: state.payload.id + 1} , logout: false};
    case constants.TEAMS_GET_START:
      return { ...state, isLoading : true, logout: false};
    case constants.TEAMS_GET_SUCCESS:
   
      return { ...state, isLoading: false, teams: action.payload.data, logout: false};
    case constants.TEAMS_GET_ERROR:
    console.log("sdsd");
      return { ...state, isLoading: false, isError: true, logout: true};
    default:
      return state;
  }
};