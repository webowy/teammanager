import * as constants from '../constants';

const initalState = {
    
    isError: false,
    isLoading: false,
    payload: { 
      data: 0
    }
};

export const paymentReducer = (state = initalState, action) => {
  switch (action.type) {
    case constants.PAYMENT_POST_START:
      return { ...state, isLoading : true};
    case constants.PAYMENT_POST_SUCCESS:
      return { ...state, isLoading: false, payment: action.payload.data};
    case constants.PAYMENT_POST_ERROR:
      return { ...state, isLoading: false, isError: true};
    default:
      return state;
  }
};