import * as constants from '../constants';

const initalState = {
    obligations: [],
    isError: false,
    isLoading: false,
    payload: { 
      id: 0
    }
};

export const obligationsReducer = (state = initalState, action) => {
  switch (action.type) {
    case constants.OBLIGATIONS_GET_START:
      return { ...state, isLoading : true};
    case constants.OBLIGATIONS_GET_SUCCESS:
      return { ...state, isLoading: false, obligations: action.payload.data};
    case constants.OBLIGATIONS_GET_ERROR:
      return { ...state, isLoading: false, isError: true};
    default:
      return state;
  }
};