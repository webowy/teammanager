import { combineReducers } from 'redux';

import { teamsReducer } from './teamsReducer';
import { playersReducer } from './playersReducer';
import { playerReducer } from './playerReducer';
import { obligationsReducer } from './obligationsReducer';
import { obligationReducer } from './obligationReducer';
import { paymentReducer } from './paymentReducer';
import { loginReducer } from './loginReducer';

const rootReducer = combineReducers({
  teamsStore: teamsReducer,
  playersStore: playersReducer,
  playerStore: playerReducer,
  obligationsStore: obligationsReducer,
  obligationStore: obligationReducer,
  paymentStore: paymentReducer,
  loginStore: loginReducer
});

export default rootReducer;
