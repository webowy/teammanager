import * as constants from '../constants';

const initalState = {
    obligation: {
      payments: []
    },
    isError: false,
    isLoading: false,
    payload: { 
      id: 0
    }
};

export const obligationReducer = (state = initalState, action) => {
  switch (action.type) {
    case constants.OBLIGATION_GET_START:
      return { ...state, isLoading : true};
    case constants.OBLIGATION_GET_SUCCESS:
      return { ...state, isLoading: false, obligation: action.payload.data};
    case constants.OBLIGATION_GET_ERROR:
      return { ...state, isLoading: false, isError: true};
    case constants.OBLIGATION_UPDATE:
      const payments = state.obligation.payments;
      payments.push({
        id:action.payload.data.id,
        createDate : action.payload.data.create_date,
        type:action.payload.data.type,
        amount: action.payload.data.amount
      });

      return { ...state, isLoading: false, isError: false, 
        obligation: Object.assign( state.obligation, { payments })};
    default:
      return state;
  }
};