import React from 'react';
import { connect } from 'react-redux';
import * as actions from '../actions/login';
import { NavLink } from 'react-router-dom';
import PropTypes from 'prop-types';
import { withStyles } from 'material-ui/styles';
import Drawer from 'material-ui/Drawer';
import Divider from 'material-ui/Divider';
import List, { ListItem, ListItemIcon, ListItemText } from 'material-ui/List';
import ChevronLeftIcon from 'material-ui-icons/ChevronLeft';
import ChevronRightIcon from 'material-ui-icons/ChevronRight';
import DashboardIcon from 'material-ui-icons/Dashboard';
import DirectionsRunIcon from 'material-ui-icons/DirectionsRun';
import SettingsIcon from 'material-ui-icons/Settings';
import ExitToApp from 'material-ui-icons/ExitToApp';
import IconButton from 'material-ui/IconButton';
import * as userManager from './../User/userManager';
import { Redirect } from 'react-router';

import mainStyles from './../styles/mainStyles';


import classNames from 'classnames';

class SideBar extends React.Component { 

    static contextTypes = {
        router: PropTypes.object
    }

    state = {
        redirect: null
    }

    logout() {
        userManager.logout();
        this.props.setLogout();
        this.context.router.history.push('/login');
    }

      
    render() {
        const { classes, theme } = this.props;
        
        return (
            <Drawer
                        type="permanent"
                        classes={{
                        paper: classNames(classes.drawerPaper, !this.props.open && classes.drawerPaperClose),
                        }}
                        open={this.props.open}
                    >
                        {this.state.redirect}
                        <div className={classes.drawerInner}>
                        <div className={classes.drawerHeader}>
                            <IconButton onClick={this.props.handleDrawerClose}>
                            {theme.direction === 'rtl' ? <ChevronRightIcon /> : <ChevronLeftIcon />}
                            </IconButton>
                        </div>
                        <Divider />
                        <List className={classes.list}>
                            <NavLink to="" className={classes.menuLink}>
                            <ListItem button>
                                <ListItemIcon>
                                    <DashboardIcon />
                                </ListItemIcon>
                                <ListItemText primary="Dashboard"  />
                            </ListItem>
                            </NavLink>
                            <NavLink to="/teams" className={classes.menuLink}>
                            <ListItem button>
                                <ListItemIcon>
                                <DirectionsRunIcon />
                                </ListItemIcon>  
                                <ListItemText primary="Teams" />
                            </ListItem>
                            </NavLink>
                        </List>
                        <Divider />
                        <List className={classes.list}>
                            <ListItem button>
                                <ListItemIcon>
                                    <SettingsIcon />
                                </ListItemIcon>
                                <ListItemText primary="Settings" />
                            </ListItem>
                            <ListItem button onClick={()=>this.logout()}>
                                <ListItemIcon>
                                    <ExitToApp />
                                </ListItemIcon>
                                <ListItemText primary="Settings" />
                            </ListItem>
                        </List>
                        </div>
                    </Drawer>
        );
    }

}
const mapStateToProps = (state) => {
    return {
      ...state
    };
  }

  const mapDispatchToProps = (dispatch) => {
    return {
        setLogout: (data) => dispatch(actions.setLogout())
    };
}
export default withStyles(mainStyles, { withTheme: true })(connect(mapStateToProps, mapDispatchToProps)(SideBar));