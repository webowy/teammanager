import React from 'react';
import { withStyles } from 'material-ui/styles';
import AppBar from 'material-ui/AppBar';
import Toolbar from 'material-ui/Toolbar';
import MenuIcon from 'material-ui-icons/Menu';
import Typography from 'material-ui/Typography';
import IconButton from 'material-ui/IconButton';
import mainStyles from './../styles/mainStyles';


import classNames from 'classnames';

class MainAppBar extends React.Component { 
      
    render() {
        const { classes } = this.props;

        return (
            <AppBar className={classNames(classes.appBar, this.props.open && classes.appBarShift)}>
                <Toolbar disableGutters={!this.props.open}>
                <IconButton
                    color="contrast"
                    aria-label="open drawer"
                    onClick={this.props.handleDrawerOpen}
                    className={classNames(classes.menuButton, this.props.open && classes.hide)}
                >
                    <MenuIcon />
                </IconButton>
                <Typography type="title" color="inherit" noWrap>
                    Team Manager
                </Typography>
                </Toolbar>
            </AppBar>
        );
    }

}

export default withStyles(mainStyles, { withTheme: true })(MainAppBar);