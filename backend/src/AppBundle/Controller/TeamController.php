<?php namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use FOS\RestBundle\Controller\Annotations;
use FOS\RestBundle\Request\ParamFetcherInterface;
use FOS\RestBundle\Controller\FOSRestController;
use Doctrine\ORM\Tools\Pagination\Paginator;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use FOS\RestBundle\View\View;
use AppBundle\Entity\Team;

class TeamController extends FOSRestController
{
    /**
     * List all licenses.
     * @Annotations\Get("/team")
     * @Annotations\QueryParam(name="_start", requirements="\d+", default=0, nullable=true, description="Start.")
     * @Annotations\QueryParam(name="_end", requirements="\d+", default=10, nullable=true, description="End.")
     * @Annotations\QueryParam(name="_sort", nullable=true, description="Sort field.")
     * @Annotations\QueryParam(name="_order", nullable=true, description="Sort direction.")
     *
     * @param Request $request the request object
     * @param ParamFetcherInterface $paramFetcher param fetcher service
     *
     * @return array
     */
    public function getTeamAction(Request $request, ParamFetcherInterface $paramFetcher)
    {
        $sortField = $paramFetcher->get('_sort');
        $sortDir = $paramFetcher->get('_order');
        $start = $paramFetcher->get('_start');
        $end = $paramFetcher->get('_end');
      


        $em = $this->getDoctrine()->getEntityManager();
        $qb = $em->createQueryBuilder();

        $qb->select('c')
            ->from('AppBundle:Team', 'c')
            ->where('c.active = true');

        if ($sortField && $sortDir) {
            $qb->orderBy('c.' . $sortField, $sortDir);
        }

        $query = $qb->getQuery();

        $query->setFirstResult($start)
            ->setMaxResults($end);

        $paginator = new Paginator($query);
        $totalCount = $paginator->count();

        $teams = $query->getResult();

        if ($teams === null) {
          return new View("there are no teams exist", Response::HTTP_NOT_FOUND);
        }


        $view = $this
            ->view($teams, 200)
            ->setHeader('X-Total-Count', $totalCount);
        return $this->handleView($view);
    } 

    /**
    * @Annotations\Get("/team/{id}")
    */
    public function idAction($id)
    {
        $singleresult = $this->getDoctrine()->getRepository('AppBundle:Team')->find($id);
        if ($singleresult === null) {
            return new View("Team not found", Response::HTTP_NOT_FOUND);
        }
        return $singleresult;
    }

    /**
    * @Annotations\Post("/team")
    */
    public function postAction(Request $request)
    {

       
        $team = new Team;
        
        $name = $request->get('name');
        $contributionVal = $request->get('contribution_val');
        $contributionPeriod = $request->get('contribution_period');
        $contributionTime = new \DateTime($request->get('contribution_time'));
        

        if(empty($name) || empty($contributionVal) || empty($contributionPeriod))
        {
            return new View("NULL VALUES ARE NOT ALLOWED", Response::HTTP_NOT_ACCEPTABLE); 
        }

        $team -> setName($name);
        $team -> setContributionVal($contributionVal);
        $team -> setContributionPeriod($contributionPeriod);
        $team -> setContributionTime($contributionTime);

        $em = $this->getDoctrine()->getManager();
        $em->persist($team);
        $em->flush();

        return new View("Team Added Successfully", Response::HTTP_OK);
    }

 /**
 * @Annotations\Put("/team/{id}")
 */
 public function updateAction($id,Request $request)
 { 
    $name = $request->get('name');
    $contributionVal = $request->get('contribution_val');
    $contributionPeriod = $request->get('contribution_period');
    $contributionTime = $request->get('contribution_time');

    $em = $this->getDoctrine()->getManager();
    $team = $this->getDoctrine()->getRepository('AppBundle:Team')->find($id);

    if (empty($team)) {
        return new View("Team not found", Response::HTTP_NOT_FOUND);
    } else {
        if (!empty($name)) {
            $team->setName($name);
            $em->persist($team);
        }
        if (!empty($contributionVal)) {
            $team->setContributionVal($contributionVal);
            $em->persist($team);
        }
        if (!empty($contributionPeriod)) {
            $team->setContributionPeriod($contributionPeriod);
            $em->persist($team);
        }
        if (!empty($contributionTime)) {
            $team->setContributionTime($contributionTime);
            $em->persist($team);
        }
        $em->flush();
        return new View("Team Updated Successfully", Response::HTTP_OK);
    }
   
   
 }

 /**
 * @Annotations\Delete("/team/{id}")
 */
 public function deleteAction($id,Request $request)
 { 
    $em = $this->getDoctrine()->getManager();
    $team = $this->getDoctrine()->getRepository('AppBundle:Team')->find($id);

    if (empty($team)) {
        return new View("Team not found", Response::HTTP_NOT_FOUND);
    } else {
        if (!empty($id)) {
            $team->setActive(false);
            $em->persist($team);
        }
       
        $em->flush();
        return new View("Team Deleted Successfully", Response::HTTP_OK);
    }

 }
}