<?php namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use FOS\RestBundle\Controller\Annotations;
use FOS\RestBundle\Request\ParamFetcherInterface;
use FOS\RestBundle\Controller\FOSRestController;
use Doctrine\ORM\Tools\Pagination\Paginator;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use FOS\RestBundle\View\View;
use AppBundle\Entity\Clubs;

class LicensesController extends FOSRestController
{
    /**
     * List all licenses.
     * @Annotations\Get("/licenses")
     * @Annotations\QueryParam(name="_start", requirements="\d+", default=0, nullable=true, description="Start.")
     * @Annotations\QueryParam(name="_end", requirements="\d+", default=10, nullable=true, description="End.")
     * @Annotations\QueryParam(name="_sort", nullable=true, description="Sort field.")
     * @Annotations\QueryParam(name="_order", nullable=true, description="Sort direction.")
     *
     * @param Request $request the request object
     * @param ParamFetcherInterface $paramFetcher param fetcher service
     *
     * @return array
     */
    public function getClubsAction(Request $request, ParamFetcherInterface $paramFetcher)
    {
        $sortField = $paramFetcher->get('_sort');
        $sortDir = $paramFetcher->get('_order');
        $start = $paramFetcher->get('_start');
        $end = $paramFetcher->get('_end');
      


        $em = $this->getDoctrine()->getEntityManager();
        $qb = $em->createQueryBuilder();

        $qb->select('c')
            ->from('AppBundle:Licenses', 'c');

        if ($sortField && $sortDir) {
            $qb->orderBy('c.' . $sortField, $sortDir);
        }

        $query = $qb->getQuery();

        $query->setFirstResult($start)
            ->setMaxResults($end);

        $paginator = new Paginator($query);
        $totalCount = $paginator->count();

        $licenses = $query->getResult();

        if ($licenses === null) {
          return new View("there are no licenses exist", Response::HTTP_NOT_FOUND);
        }


        $view = $this
            ->view($licenses, 200)
            ->setHeader('X-Total-Count', $totalCount);
        return $this->handleView($view);
    } 

    /**
    * @Annotations\Get("/licenses/{id}")
    */
    public function idAction($id)
    {
        $singleresult = $this->getDoctrine()->getRepository('AppBundle:Licenses')->find($id);
        if ($singleresult === null) {
            return new View("Licenses not found", Response::HTTP_NOT_FOUND);
        }
        return $singleresult;
    }

    /**
    * @Annotations\Post("/licenses")
    */
    public function postAction(Request $request)
    {
        $club = new Clubs;
        
        $name = $request->get('name');

        if(empty($name))
        {
            return new View("NULL VALUES ARE NOT ALLOWED", Response::HTTP_NOT_ACCEPTABLE); 
        }

        $club -> setName($name);

        $em = $this->getDoctrine()->getManager();
        $em->persist($club);
        $em->flush();

        return new View("Club Added Successfully", Response::HTTP_OK);
    }

 /**
 * @Annotations\Put("/licenses/{id}")
 */
 public function updateAction($id,Request $request)
 { 
    //$data = new Clubs;
    $name = $request->get('name');

    $sn = $this->getDoctrine()->getManager();
    $club = $this->getDoctrine()->getRepository('AppBundle:Licenses')->find($id);
    if (empty($club)) {
        return new View("Licenses not found", Response::HTTP_NOT_FOUND);
    }  elseif (!empty($name)) {
        $club->setName($name);
        $sn->flush();
        return new View("Licenses Name Updated Successfully", Response::HTTP_OK); 
    }
    return new View("Licenses name cannot be empty", Response::HTTP_NOT_ACCEPTABLE); 
 }
}