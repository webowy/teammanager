<?php namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use FOS\RestBundle\Controller\Annotations;
use FOS\RestBundle\Request\ParamFetcherInterface;
use FOS\RestBundle\Controller\FOSRestController;
use Doctrine\ORM\Tools\Pagination\Paginator;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use FOS\RestBundle\View\View;
use AppBundle\Entity\Obligation;

class ObligationController extends FOSRestController
{
    /**
     * List all licenses.
     * @Annotations\Get("/obligations")
     * @Annotations\QueryParam(name="_start", requirements="\d+", default=0, nullable=true, description="Start.")
     * @Annotations\QueryParam(name="_end", requirements="\d+", default=10, nullable=true, description="End.")
     * @Annotations\QueryParam(name="_sort", nullable=true, description="Sort field.")
     * @Annotations\QueryParam(name="_order", nullable=true, description="Sort direction.")
    * @Annotations\QueryParam(name="_player", nullable=true, description="User.")
     *
     * @param Request $request the request object
     * @param ParamFetcherInterface $paramFetcher param fetcher service
     *
     * @return array
     */
    public function getObligationsAction(Request $request, ParamFetcherInterface $paramFetcher)
    {
        $sortField = $paramFetcher->get('_sort');
        $sortDir = $paramFetcher->get('_order');
        $start = $paramFetcher->get('_start');
        $end = $paramFetcher->get('_end');
        $player = $paramFetcher->get('_player');
      


        $em = $this->getDoctrine()->getEntityManager();
        $qb = $em->createQueryBuilder();

        $qb->select('o.id, o.createDate, o.obligationDate, o.obligationValue, sum(p.amount) as payment_amount')
            ->from('AppBundle:Payment', 'p')
            ->leftJoin('p.obligation', 'o')
            ->groupby('o.id');

        if ($sortField && $sortDir) {
            $qb->orderBy('o.' . $sortField, $sortDir);
        }
        if($player) {
             $qb->where('o.player = :player')
             ->setParameter('player', $player); 
        }

        $query = $qb->getQuery();

        $query->setFirstResult($start)
            ->setMaxResults($end);

        $paginator = new Paginator($query);
        $paginator->setUseOutputWalkers(false);
        $totalCount = $paginator->count();

        $obligations = $query->getResult();

        if ($obligations === null) {
          return new View("there are no obligations exist", Response::HTTP_NOT_FOUND);
        }


        $view = $this
            ->view($obligations, 200)
            ->setHeader('X-Total-Count', $totalCount);
        return $this->handleView($view);
    } 

    /**
    * @Annotations\Get("/obligation/{id}")
    */
    public function idAction($id)
    {

        $em = $this->getDoctrine()->getEntityManager();
        $qb = $em->createQueryBuilder();

        $qb->select('o.id, o.createDate, o.obligationDate, o.obligationValue')
            ->from('AppBundle:Obligation', 'o')
            ->where('o.id = :id and o.active = true')
            ->setParameter('id', $id);
        
        $query = $qb->getQuery();
        $obligation = $query->getResult()[0];
       
        if ($obligation === null) {
            return new View("Obligation not found", Response::HTTP_NOT_FOUND);
        }
        $qbp = $em->createQueryBuilder();
        $qbp->select('p.id, p.createDate, p.type, p.amount')
            ->from('AppBundle:Payment', 'p')
            ->where('p.obligation = :ObligationId and p.active = true')
            ->setParameter('ObligationId', $id);
        
        $queryp = $qbp->getQuery();
        $payments = $queryp->getResult();
        $obligation['payments'] = $payments;
        return $obligation;
    }

    /**
    * @Annotations\Post("/obligation")
    */
    public function postAction(Request $request)
    {

        $obligation = new Obligation;
        
        $player = $this->getDoctrine()->getRepository('AppBundle:Player')->find($request->get('player'));
        $obligationDate = $request->get('obligationDate');
        $obligationValue = $request->get('obligationValue');
        

        if(empty($player))
        {
            return new View("NULL player ARE NOT ALLOWED", Response::HTTP_NOT_ACCEPTABLE); 
        }
        if(empty($obligationDate))
        {
            return new View("NULL obligationDate ARE NOT ALLOWED", Response::HTTP_NOT_ACCEPTABLE); 
        }
        if(empty($obligationValue))
        {
            return new View("NULL obligationValue ARE NOT ALLOWED", Response::HTTP_NOT_ACCEPTABLE); 
        }

        $obligation -> setPlayer($player);
        $obligation -> setCreateDate(new \DateTime());
        $obligation -> setObligationDate(new \DateTime($obligationDate));
        $obligation -> setObligationValue($obligationValue);
        
        

        $em = $this->getDoctrine()->getManager();
        $em->persist($obligation);
        $em->flush();

        return new View("Obligation Added Successfully", Response::HTTP_OK);
    }

 /**
 * @Annotations\Put("/obligation/{id}")
 */
 public function updateAction($id,Request $request)
 { 
    $player = $this->getDoctrine()->getRepository('AppBundle:Player')->find($request->get('player'));
    $obligationDate = $request->get('obligationDate');
    $obligationValue = $request->get('obligationValue');

    $em = $this->getDoctrine()->getManager();
    $obligation = $this->getDoctrine()->getRepository('AppBundle:Obligation')->find($id);

    if (empty($obligation)) {
        return new View("obligation not found", Response::HTTP_NOT_FOUND);
    } else {
        if (!empty($player)) {
            $obligation -> setPlayer($player);
        }
        if (!empty($obligationDate)) {
            $obligation -> setObligationDate(new \DateTime($obligationDate));
        }
        if (!empty($obligationValue)) {
            $obligation -> setObligationValue($obligationValue); 
        }
       
        $em->persist($obligation);
        $em->flush();
        return new View("obligation Updated Successfully", Response::HTTP_OK);
    }   
 }

 /**
 * @Annotations\Delete("/obligation/{id}")
 */
 public function deleteAction($id,Request $request)
 { 
    $em = $this->getDoctrine()->getManager();
    $obligation = $this->getDoctrine()->getRepository('AppBundle:Obligation')->find($id);

    if (empty($obligation)) {
        return new View("Obligation not found", Response::HTTP_NOT_FOUND);
    } else {
        if (!empty($id)) {
            $obligation->setActive(false);
            $em->persist($obligation);
        }
       
        $em->flush();
        return new View("Obligation Deleted Successfully", Response::HTTP_OK);
    }

 }
}