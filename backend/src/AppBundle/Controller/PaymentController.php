<?php namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use FOS\RestBundle\Controller\Annotations;
use FOS\RestBundle\Request\ParamFetcherInterface;
use FOS\RestBundle\Controller\FOSRestController;
use Doctrine\ORM\Tools\Pagination\Paginator;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use FOS\RestBundle\View\View;
use AppBundle\Entity\Payment;

class PaymentController extends FOSRestController
{
    /**
     * List all licenses.
     * @Annotations\Get("/payments")
     * @Annotations\QueryParam(name="_start", requirements="\d+", default=0, nullable=true, description="Start.")
     * @Annotations\QueryParam(name="_end", requirements="\d+", default=10, nullable=true, description="End.")
     * @Annotations\QueryParam(name="_sort", nullable=true, description="Sort field.")
     * @Annotations\QueryParam(name="_order", nullable=true, description="Sort direction.")
    * @Annotations\QueryParam(name="_player", nullable=true, description="User.")
     *
     * @param Request $request the request object
     * @param ParamFetcherInterface $paramFetcher param fetcher service
     *
     * @return array
     */
    public function getPaymentsAction(Request $request, ParamFetcherInterface $paramFetcher)
    {
        $sortField = $paramFetcher->get('_sort');
        $sortDir = $paramFetcher->get('_order');
        $start = $paramFetcher->get('_start');
        $end = $paramFetcher->get('_end');
        $player = $paramFetcher->get('_player');
      


        $em = $this->getDoctrine()->getEntityManager();
        $qb = $em->createQueryBuilder();

        $qb->select('p.id, o.createDate as ObligationDate, o.obligationValue as ObligationAmount, p.amount, p.type, p.createDate, p.createDate')
            ->from('AppBundle:Payment', 'p')
            ->join('p.obligation', 'o');

         $qb->where('p.active = true');


        if ($sortField && $sortDir) {
            $qb->orderBy('p.' . $sortField, $sortDir);
        }
        // if($player) {
        //      $qb->where('p.player = :player and p.active = true')
        //      ->setParameter('player', $player); 
        // }

        $query = $qb->getQuery();

        $query->setFirstResult($start)
            ->setMaxResults($end);

        $paginator = new Paginator($query);
        $paginator->setUseOutputWalkers(false);
        $totalCount = $paginator->count();

        $obligations = $query->getResult();

        if ($obligations === null) {
          return new View("there are no payments exist", Response::HTTP_NOT_FOUND);
        }


        $view = $this
            ->view($obligations, 200)
            ->setHeader('X-Total-Count', $totalCount);
        return $this->handleView($view);
    } 

    /**
    * @Annotations\Get("/payment/{id}")
    */
    public function idAction($id)
    {

        $em = $this->getDoctrine()->getEntityManager();
        $qb = $em->createQueryBuilder();

        $qb->select('p.id, o.createDate as ObligationDate, o.obligationValue as ObligationAmount, p.amount, p.type, p.createDate, p.createDate')
            ->from('AppBundle:Payment', 'p')
            ->join('p.obligation', 'o')
            ->where('p.id = :id and p.active = true')
            ->setParameter('id', $id);
        
        $query = $qb->getQuery();
        $obligation = $query->getResult();
       
        if ($obligation === null) {
            return new View("Obligation not found", Response::HTTP_NOT_FOUND);
        }
        return $obligation;
    }

    /**
    * @Annotations\Post("/payment")
    */
    public function postAction(Request $request)
    {

        $payment = new Payment;
        
        $obligation = $this->getDoctrine()->getRepository('AppBundle:Obligation')->find($request->get('obligation'));
        $amount = $request->get('amount');
        $type = $request->get('type');
        

        if(empty($obligation))
        {
            return new View("NULL obligation ARE NOT ALLOWED", Response::HTTP_NOT_ACCEPTABLE); 
        }
        if(empty($amount))
        {
            return new View("NULL amount ARE NOT ALLOWED", Response::HTTP_NOT_ACCEPTABLE); 
        }
        if(empty($type))
        {
            return new View("NULL type ARE NOT ALLOWED", Response::HTTP_NOT_ACCEPTABLE); 
        }

        $payment -> setObligation($obligation);
        $payment -> setCreateDate(new \DateTime());
        $payment -> setAmount($amount);
        $payment -> setType($type);
        
        

        $em = $this->getDoctrine()->getManager();
        $em->persist($payment);
        $em->flush();

        return new View($payment, Response::HTTP_OK);
    }

 /**
 * @Annotations\Put("/payment/{id}")
 */
 public function updateAction($id,Request $request)
 { 
    $obligation = $this->getDoctrine()->getRepository('AppBundle:Obligation')->find($request->get('obligation'));
    $amount = $request->get('amount');
    $type = $request->get('type');

    $em = $this->getDoctrine()->getManager();
    $payment = $this->getDoctrine()->getRepository('AppBundle:Payment')->find($id);

    if (empty($payment)) {
        return new View("Payment not found", Response::HTTP_NOT_FOUND);
    } else {
        if (!empty($obligation)) {
            $payment -> setObligation($obligation);
        }
        if (!empty($amount)) {
            $payment -> setAmount($amount);
        }
        if (!empty($type)) {
            $payment -> setType($type); 
        }
       
        $em->persist($payment);
        $em->flush();
        return new View("Payment Updated Successfully", Response::HTTP_OK);
    }   
 }

 /**
 * @Annotations\Delete("/payment/{id}")
 */
 public function deleteAction($id,Request $request)
 { 
    $em = $this->getDoctrine()->getManager();
    $payment = $this->getDoctrine()->getRepository('AppBundle:Payment')->find($id);

    if (empty($payment)) {
        return new View("Payment not found", Response::HTTP_NOT_FOUND);
    } else {
        if (!empty($id)) {
            $payment->setActive(false);
            $em->persist($payment);
        }
       
        $em->flush();
        return new View("Payment Deleted Successfully", Response::HTTP_OK);
    }

 }
}