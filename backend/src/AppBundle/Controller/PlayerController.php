<?php namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use FOS\RestBundle\Controller\Annotations;
use FOS\RestBundle\Request\ParamFetcherInterface;
use FOS\RestBundle\Controller\FOSRestController;
use Doctrine\ORM\Tools\Pagination\Paginator;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use FOS\RestBundle\View\View;
use AppBundle\Entity\Player;

class PlayerController extends FOSRestController
{
    /**
     * List all licenses.
     * @Annotations\Get("/players")
     * @Annotations\QueryParam(name="_start", requirements="\d+", default=0, nullable=true, description="Start.")
     * @Annotations\QueryParam(name="_end", requirements="\d+", default=10, nullable=true, description="End.")
     * @Annotations\QueryParam(name="_sort", nullable=true, description="Sort field.")
     * @Annotations\QueryParam(name="_order", nullable=true, description="Sort direction.")
     * @Annotations\QueryParam(name="_team", nullable=true, description="Team.")
     *
     * @param Request $request the request object
     * @param ParamFetcherInterface $paramFetcher param fetcher service
     *
     * @return array
     */
    public function getPlayersAction(Request $request, ParamFetcherInterface $paramFetcher)
    {
        $sortField = $paramFetcher->get('_sort');
        $sortDir = $paramFetcher->get('_order');
        $start = $paramFetcher->get('_start');
        $end = $paramFetcher->get('_end');
        $team = $paramFetcher->get('_team');
      


        $em = $this->getDoctrine()->getEntityManager();
        $qb = $em->createQueryBuilder();

        $qb->select('c.id, c.name, c.surname, c.birth, c.street, c.streetNumber, c.city, c.postCode, c.createDate')
            ->from('AppBundle:Player', 'c')
            ->where('c.active = true');

        if($team) {
            $qb->where('c.active = true and c.team = '.$team);
        }

        if ($sortField && $sortDir) {
            $qb->orderBy('c.' . $sortField, $sortDir);
        }

        $query = $qb->getQuery();

        $query->setFirstResult($start)
            ->setMaxResults($end);

        $paginator = new Paginator($query);
        $paginator->setUseOutputWalkers(false);
        $totalCount = $paginator->count();

        $players = $query->getResult();

        if ($players === null) {
          return new View("there are no players exist", Response::HTTP_NOT_FOUND);
        }


        $view = $this
            ->view($players, 200)
            ->setHeader('X-Total-Count', $totalCount);
        return $this->handleView($view);
    } 

    /**
    * @Annotations\Get("/player/{id}")
    */
    public function idAction($id)
    {

        $em = $this->getDoctrine()->getEntityManager();
        $qb = $em->createQueryBuilder();

        $qb->select('c.id, c.name, c.surname, c.birth, c.street, c.streetNumber, c.city, c.postCode, c.createDate')
            ->from('AppBundle:Player', 'c')
            ->where('c.id = :id')
            ->setParameter('id', $id);
        
        $query = $qb->getQuery();
        $player = $query->getResult();
       
        if ($player === null) {
            return new View("Player not found", Response::HTTP_NOT_FOUND);
        }
        return $player;
    }

    /**
    * @Annotations\Post("/player")
    */
    public function postAction(Request $request)
    {

        $player = new Player;
        
        $name = $request->get('name');
        $surname = $request->get('surname');
        $birth = $request->get('birth');
        $street = $request->get('street');
        $streetNumber = $request->get('streetNumber');
        $city = $request->get('city');
        $postCode = $request->get('postCode');
        $user = $this->getDoctrine()->getRepository('AppBundle:Users')->find($request->get('user'));
        $team = $this->getDoctrine()->getRepository('AppBundle:Team')->find($request->get('team'));
        

        if(empty($name))
        {
            return new View("NULL NAME ARE NOT ALLOWED", Response::HTTP_NOT_ACCEPTABLE); 
        }
        if(empty($surname))
        {
            return new View("NULL surname ARE NOT ALLOWED", Response::HTTP_NOT_ACCEPTABLE); 
        }
        if(empty($user))
        {
            return new View("NULL user ARE NOT ALLOWED", Response::HTTP_NOT_ACCEPTABLE); 
        }
        if(empty($team))
        {
            return new View("NULL team ARE NOT ALLOWED", Response::HTTP_NOT_ACCEPTABLE); 
        }

        $player -> setName($name);
        $player -> setSurname($surname); 
        $player -> setBirth(new \DateTime($birth));
        $player -> setStreet($street);
        $player -> setStreetNumber($streetNumber);
        $player -> setCity($city);
        $player -> setPostCode($postCode);
        $player -> setUser($user);
        $player -> setTeam($team);
        $player -> setCreateDate(new \DateTime());
        

        $em = $this->getDoctrine()->getManager();
        $em->persist($player);
        $em->flush();

        return new View("Player Added Successfully", Response::HTTP_OK);
    }

 /**
 * @Annotations\Put("/player/{id}")
 */
 public function updateAction($id,Request $request)
 { 
    $name = $request->get('name');
    $surname = $request->get('surname');
    $birth = $request->get('birth');
    $street = $request->get('street');
    $streetNumber = $request->get('streetNumber');
    $city = $request->get('city');
    $postCode = $request->get('postCode');
    $user = $this->getDoctrine()->getRepository('AppBundle:Users')->find($request->get('user'));
    $team = $this->getDoctrine()->getRepository('AppBundle:Team')->find($request->get('team'));

    $em = $this->getDoctrine()->getManager();
    $player = $this->getDoctrine()->getRepository('AppBundle:Player')->find($id);

    if (empty($player)) {
        return new View("player not found", Response::HTTP_NOT_FOUND);
    } else {
        if (!empty($name)) {
            $player->setName($name);
        }
        if (!empty($surname)) {
            $player -> setSurname($surname); 
        }
        if (!empty($birth)) {
            $player -> setBirth(new \DateTime($birth));
        }
        if (!empty($street)) {
            $player -> setStreet($street); 
        }
        if (!empty($streetNumber)) {
            $player -> setStreetNumber($streetNumber); 
        }
        if (!empty($city)) {
            $player -> setCity($city); 
        }
        if (!empty($postCode)) {
            $player -> setPostCode($postCode); 
        }
        if (!empty($user)) {
            $player -> setUser($user); 
        }
         if (!empty($team)) {
            $player -> setTeam($team);  
        }

        $em->persist($player);
        $em->flush();
        return new View("Player Updated Successfully", Response::HTTP_OK);
    }
   
   
 }

 /**
 * @Annotations\Delete("/player/{id}")
 */
 public function deleteAction($id,Request $request)
 { 
    $em = $this->getDoctrine()->getManager();
    $player = $this->getDoctrine()->getRepository('AppBundle:Player')->find($id);

    if (empty($player)) {
        return new View("Player not found", Response::HTTP_NOT_FOUND);
    } else {
        if (!empty($id)) {
            $player->setActive(false);
            $em->persist($player);
        }
       
        $em->flush();
        return new View("Player Deleted Successfully", Response::HTTP_OK);
    }

 }
}