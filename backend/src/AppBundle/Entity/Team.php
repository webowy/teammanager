<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Team
 *
 * @ORM\Table(name="team")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\TeamRepository")
 */
class Team
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255)
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(name="contribution_val", type="decimal", precision=10, scale=0, nullable=true)
     */
    private $contributionVal;

    /**
     * @var string
     *
     * @ORM\Column(name="contribution_period", type="string", length=255)
     */
    private $contributionPeriod;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="contribution_time", type="date")
     */
    private $contributionTime;

    /**
     * @var boolean
     *
     * @ORM\Column(name="active", type="boolean", nullable=false)
     */
    private $active = true;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return Team
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set contributionVal
     *
     * @param string $contributionVal
     *
     * @return Team
     */
    public function setContributionVal($contributionVal)
    {
        $this->contributionVal = $contributionVal;

        return $this;
    }

    /**
     * Get contributionVal
     *
     * @return string
     */
    public function getContributionVal()
    {
        return $this->contributionVal;
    }

    /**
     * Set contributionPeriod
     *
     * @param string $contributionPeriod
     *
     * @return Team
     */
    public function setContributionPeriod($contributionPeriod)
    {
        $this->contributionPeriod = $contributionPeriod;

        return $this;
    }

    /**
     * Get contributionPeriod
     *
     * @return string
     */
    public function getContributionPeriod()
    {
        return $this->contributionPeriod;
    }

    /**
     * Set contributionTime
     *
     * @param \DateTime $contributionTime
     *
     * @return Team
     */
    public function setContributionTime($contributionTime)
    {
        $this->contributionTime = $contributionTime;

        return $this;
    }

    /**
     * Get contributionTime
     *
     * @return \DateTime
     */
    public function getContributionTime()
    {
        return $this->contributionTime;
    }

    /**
     * Set active
     *
     * @param boolean $active
     *
     * @return Tests
     */
    public function setActive($active)
    {
        $this->active = $active;

        return $this;
    }

    /**
     * Get active
     *
     * @return boolean
     */
    public function getActive()
    {
        return $this->active;
    }
}

