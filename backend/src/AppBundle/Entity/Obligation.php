<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Obligation
 *
 * @ORM\Table(name="obligation")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\ObligationRepository")
 */
class Obligation
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var \AppBundle\Entity\Player
     *
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Player")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="player_id", referencedColumnName="id")
     * })
     */
    private $player;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="create_date", type="date")
     */
    private $createDate;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="obligation_date", type="date")
     */
    private $obligationDate;

    /**
     * @var string
     *
     * @ORM\Column(name="obligation_value", type="decimal", precision=10, scale=0)
     */
    private $obligationValue;

    /**
     * @var boolean
     *
     * @ORM\Column(name="active", type="boolean", nullable=false)
     */
    private $active = true;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set player
     *
     * @param \AppBundle\Entity\Player $player
     *
     * @return Clubs
     */
    public function setPlayer(\AppBundle\Entity\Player $player = null)
    {
        $this->player = $player;

        return $this;
    }

    /**
     * Get player
     *
     * @return \AppBundle\Entity\Player
     */
    public function getPlayer()
    {
        return $this->player;
    }

    /**
     * Set createDate
     *
     * @param \DateTime $createDate
     *
     * @return Obligation
     */
    public function setCreateDate($createDate)
    {
        $this->createDate = $createDate;

        return $this;
    }

    /**
     * Get createDate
     *
     * @return \DateTime
     */
    public function getCreateDate()
    {
        return $this->createDate;
    }

    /**
     * Set obligationDate
     *
     * @param \DateTime $obligationDate
     *
     * @return Obligation
     */
    public function setObligationDate($obligationDate)
    {
        $this->obligationDate = $obligationDate;

        return $this;
    }

    /**
     * Get obligationDate
     *
     * @return \DateTime
     */
    public function getObligationDate()
    {
        return $this->obligationDate;
    }

    /**
     * Set obligationValue
     *
     * @param string $obligationValue
     *
     * @return Obligation
     */
    public function setObligationValue($obligationValue)
    {
        $this->obligationValue = $obligationValue;

        return $this;
    }

    /**
     * Get obligationValue
     *
     * @return string
     */
    public function getObligationValue()
    {
        return $this->obligationValue;
    }

    /**
     * Set active
     *
     * @param boolean $active
     *
     * @return Obligation
     */
    public function setActive($active)
    {
        $this->active = $active;

        return $this;
    }

    /**
     * Get active
     *
     * @return boolean
     */
    public function getActive()
    {
        return $this->active;
    }
}

