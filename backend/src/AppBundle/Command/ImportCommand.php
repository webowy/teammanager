<?php namespace AppBundle\Command;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;

class ImportCommand extends ContainerAwareCommand
{
    protected function configure()
    {
        $this
        ->setName('import')
        ->setDescription('Importing licences.');
    }
    protected function execute(InputInterface $input, OutputInterface $output)
    {
      
      $import = $this->getContainer()->get('licences');
      $licenses = $import->import();

    }
}