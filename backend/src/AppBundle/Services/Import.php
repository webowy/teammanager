<?php
namespace AppBundle\Services;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorage;
use AppBundle\Entity\Licenses;
use AppBundle\Entity\Users;
use Doctrine\ORM\EntityManager; 
use Symfony\Component\HttpFoundation\RequestStack;


class Import
{
    
    protected $em;

    public function __construct(EntityManager $em)
    {
        $this->em = $em;
    }

    public function import()
    {

         print_r("Importing!");

        $connection = $this->em->getConnection();

        $statement = $connection->prepare("
         SELECT * FROM licencje WHERE id_licencji");
        $statement->execute();
        $results = $statement->fetchAll();
         
        $this->em->flush();

        
         
         foreach($results as $result) {

            
            // print_r($result);
            $license = new Licenses();
            $license -> setName($result["imie"]);
            $license -> setSurname($result["nazwisko"]);
            $license -> setBirth(new \DateTime($result["data_urodzenia"]));
            $license -> setBirthPlace($result["miejsceurodzenia"]);
            $license -> setPesel($result["pesel"]);
            $license -> setProvince($result["wojewodztwo"]);
            $license -> setStreet($result["ulica"]);
            $license -> setStreetNumber($result["nr"]);
            $license -> setCity($result["miasto"]);
            $license -> setPostCode($result["kod"]);
            $license -> setPostalStreet($result["ulicakor"]);
            $license -> setPostalStreetNumber($result["nrkor"]);
            $license -> setPostalCity($result["miastokor"]);
            $license -> setPostalPostCode($result["kodkor"]);
            $license -> setIsClub(($result["klubowa"] == "tak") ? "true" : "false");
            $license -> setIsProfessional(($result["zawodowa"] == "tak") ? "true" : "false");
            $license -> setNationality($result["narodowosc"]);
            $license -> setParentFirst($result["rodzic1"]);
            $license -> setParentSecond($result["rodzic2"]);
            $license -> setPhotoUrl($result["url"]);
            $license -> setType($result["rodzaj_licencji"]);
            $license -> setStatus($result["status"]);
            $license -> setRequestUrl($result["url_wniosek"]);
            $club =  $this->em->getRepository('AppBundle:Clubs')->find($result["klub"]);
            $license -> setClub($club);

            $this->em->persist($license); 
         

         }

         $this->em->flush();

    }
}
